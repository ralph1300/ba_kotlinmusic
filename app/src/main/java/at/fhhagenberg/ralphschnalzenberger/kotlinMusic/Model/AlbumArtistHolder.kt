package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.ImageLoader
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R

/**
 * Created by ralphschnalzenberger on 28/11/2016.
 */
class AlbumArtistHolder<in T>(itemView: View?, val itemClick: (T) -> Unit) : RecyclerView.ViewHolder(itemView) {
    /**
    * binds an artist or an album
     * @param element the artist/album to bind to the view
     */
    fun bindArtistOrAlbum(element: T) {
        val textView = itemView.findViewById(R.id.nameTextView) as TextView
        val infoText = itemView.findViewById(R.id.infoTextView) as TextView
        val infoText2 = itemView.findViewById(R.id.info2TextView) as TextView
        val imgView = itemView.findViewById(R.id.imageView) as ImageView
        when (element) {
            is Album -> {
                if (element.image?.resource_url != null) ImageLoader.loadImage(element.image!!.resource_url, imgView)
                textView.text = element.title
                infoText.text = element.country
                infoText2.text = element.catno
            }
            is Artist -> {
                if (element.image?.resource_url != null) ImageLoader.loadImage(element.image!!.resource_url, imgView)
                textView.text = element.name
                val placeHolder = "Members: ${element.members?.size}"
                infoText.text = placeHolder
                infoText2.visibility = View.INVISIBLE
            }
            else -> {
            }
        }
        itemView.setOnClickListener { itemClick(element) }
    }
}