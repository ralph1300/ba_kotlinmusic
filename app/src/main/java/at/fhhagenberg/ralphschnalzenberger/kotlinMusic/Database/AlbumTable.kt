package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.database.sqlite.SQLiteDatabase

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 */
/**
 * representation of the album table
 */
class AlbumTable {

    companion object {
        val TABLE_ALBUM = "album"
        val COLUMN_ID = "id"
        val COLUMN_ARTISTID = "artistID"
        val COLUMN_TITLE = "title"
        val COLUMN_CATNO = "catno"
        val COLUMN_YEAR = "year"
        val COLUMN_RESOURCEURL = "resource_url"
        val COLUMN_COUNTRY = "country"
        val COLUMN_GENRE = "genre"
        val COLUMN_IMAGE = "image"
        val COLUMN_THUMB = "thumb"



        private val DATABASE_CREATE = "create table $TABLE_ALBUM (" +
                "$COLUMN_ID integer primary key," +
                "$COLUMN_ARTISTID integer," +
                "$COLUMN_TITLE text," +
                "$COLUMN_CATNO text," +
                "$COLUMN_YEAR text," +
                "$COLUMN_RESOURCEURL text," +
                "$COLUMN_IMAGE text," +
                "$COLUMN_COUNTRY text," +
                "$COLUMN_GENRE text," +
                "$COLUMN_THUMB text);"


        fun onCreate(database: SQLiteDatabase) {
            database.execSQL(DATABASE_CREATE)
        }

        fun onUpgrade(database: SQLiteDatabase){
            database.execSQL("DROP TABLE IF EXISTS $TABLE_ALBUM");
            onCreate(database);
        }

        fun dropTable(database: SQLiteDatabase) {
            database.execSQL("DROP TABLE IF EXISTS $TABLE_ALBUM");
        }
    }

}

/*
* data class Album(var image: Image,
                 val artistID: String?,
                 var songs: List<Song>,
                 val id: Int,
                 val title: String,
                 val catno: String?,
                 val year: String?,
                 val resource_url: String?,
                 val country: String?,
                 val genre: List<String>,
                 val thumb: String?) : Serializable {

}
* */