package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model.SearchResultAdapter
import org.jetbrains.anko.act
import org.jetbrains.anko.intentFor

class SearchResultPreviewActivity : AppCompatActivity() {

    lateinit var searchResultList: List<Album>
    lateinit var searchResultListView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_result_preview)
        title = getString(R.string.choose)
        searchResultListView = findViewById(R.id.searchResultListView) as RecyclerView

        searchResultList = (intent.getSerializableExtra(Constants.SEARCH_RESULTS) as List<*>).filterIsInstance<Album>()

        searchResultListView.setHasFixedSize(true)
        val llm = LinearLayoutManager(act)
        llm.orientation = LinearLayoutManager.VERTICAL
        searchResultListView.layoutManager = llm
        showList()
    }

    private fun showList() {
        searchResultListView.adapter = SearchResultAdapter(act,searchResultList) { album ->
            startActivity(intentFor<AlbumPreviewActivity>().putExtra(Constants.ALBUM_PREVIEW,album))
        }
    }
}
