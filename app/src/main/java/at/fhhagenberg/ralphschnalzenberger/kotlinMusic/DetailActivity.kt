package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Member
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database.MusicDataSource
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model.ListAdapter
import org.jetbrains.anko.*
import java.io.Serializable

class DetailActivity<T> : AppCompatActivity() {

    var obj: Serializable? = null
    lateinit var titleTextView: TextView
    lateinit var infoTextView: TextView
    lateinit var tableHeader: TextView
    lateinit var infoSecTextView: TextView
    lateinit var overviewImageView: ImageView
    lateinit var listView: ListView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        tableHeader = (findViewById(R.id.tableHeader) as TextView)
        infoTextView = (findViewById(R.id.infoTextView) as TextView)
        titleTextView = (findViewById(R.id.titleTextView) as TextView)
        infoSecTextView = (findViewById(R.id.infoSecTextView) as TextView)
        overviewImageView = (findViewById(R.id.overviewImageView) as ImageView)
        listView = (findViewById(R.id.listView) as ListView)

        obj = intent.getSerializableExtra(Constants.DETAIL)
        setupUI(obj)
    }

    /**
    * sets up the ui for an object
     * @param obj the object
     *
     */
    private fun setupUI(obj: Serializable?) {
        title = "Detail"

        when(obj) {
            is Album -> {
                tableHeader.text = getString(R.string.songs)
                setupUIForAlbum(obj)
            }
            is Artist -> {
                tableHeader.text = getString(R.string.member)
                setupUIForArtist(obj)
            }
            else -> {
                return
            }
        }
    }

    /**
    * sets up the ui for an artist
     * @param artist the artist
     */
    private fun setupUIForArtist(artist: Artist) {
        infoTextView.movementMethod = ScrollingMovementMethod.getInstance()
        with(artist) {
            titleTextView.text = name
            infoTextView.text = profile
            infoSecTextView.visibility = View.INVISIBLE
            ImageLoader.loadImage(image?.resource_url, overviewImageView)
            val adapter = ListAdapter<Member>(ctx, R.layout.listitem, members)
            listView.adapter = adapter
        }
    }

    /**
    * sets up the ui for an album
     * @param album the album
     */
    private fun setupUIForAlbum(album: Album) {
        with(album) {
            titleTextView.text = title
            (findViewById(R.id.infoTextView) as TextView).text = country
            infoSecTextView.text = year
            ImageLoader.loadImage(image?.resource_url, overviewImageView)
            val adapter = ListAdapter<Song>(ctx, R.layout.listitem, songs)
            listView.adapter = adapter
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.deletemenu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_delete -> {
                createDialog()
                return true
            }
            else -> {
                //do nothing
                return true
            }
        }
    }

    /**
    * creats a dialog
     */
    private fun createDialog() {
        if (obj != null) {
            val isAlbum = obj is Album

            alert(if(isAlbum) (obj as Album).title else (obj as Artist).name) {
                customView {
                    verticalLayout {
                        val deleteText = textView {

                            text = getString(if(isAlbum) R.string.deleteAlbum else R.string.deleteArtist)
                        }.lparams(width = wrapContent) {
                            horizontalMargin = dip(16)
                        }
                        positiveButton(getString(R.string.yes)) {
                            deleteObject(obj!!)
                            finish()
                        }
                        negativeButton(getString(R.string.no)) { }
                    }
                }
            }.show()

        }
    }

    /**
    * deletes an object
     * @param obj the object to delete
     */
    private fun deleteObject(obj: Serializable) {
        val dbHelper = MusicDataSource.MusicDataSourceManager.musicDataSource
        if (dbHelper != null) {
            if (obj is Album) {
                dbHelper.deleteAlbum(obj.id)
            } else if(obj is Artist) {
                dbHelper.deleteArtist(obj.id)
            }
        }
    }
}
