package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.database.sqlite.SQLiteDatabase

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 */
/**
 * represents the artist table
 */
class ArtistTable {

    companion object {
        val TABLE_ARTIST = "artist"
        val COLUMN_ID = "id"
        val COLUMN_RESURL = "resourceURL"
        val COLUMN_NAME = "name"
        val COLUMN_RELEASEURL = "releaseURL"
        val COLUMN_PROFILE = "profile"
        val COLUMN_IMAGE = "image"

        private val DATABASE_CREATE = "create table $TABLE_ARTIST (" +
                "$COLUMN_ID integer primary key," +
                "$COLUMN_RESURL text," +
                "$COLUMN_IMAGE text," +
                "$COLUMN_RELEASEURL text," +
                "$COLUMN_PROFILE text," +
                "$COLUMN_NAME text)"


        fun onCreate(database: SQLiteDatabase) {
            database.execSQL(DATABASE_CREATE)
        }

        fun onUpgrade(database: SQLiteDatabase){
            database.execSQL("DROP TABLE IF EXISTS $TABLE_ARTIST");
            onCreate(database);
        }
        fun dropTable(database: SQLiteDatabase) {
            database.execSQL("DROP TABLE IF EXISTS $TABLE_ARTIST");
        }
    }
}