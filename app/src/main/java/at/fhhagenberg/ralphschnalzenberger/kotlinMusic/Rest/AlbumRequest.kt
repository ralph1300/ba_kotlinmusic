package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest

import android.util.Log
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Image
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Constants
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson

/**
 * Created by ralphschnalzenberger on 19/11/2016.
 */
class AlbumRequest(val albumPreview: Album) : Command<Album?>, APICalls() {

    private companion object {
        //URL-Builder
        private fun URL_REQUEST_FULL_ALBUM(id: String): String = "$BASEURL/$RELEASE$id?$AUTH_STRING"
    }

    override fun execute(callback: (Album?) -> Unit) {

        val url = URL_REQUEST_FULL_ALBUM(albumPreview.id.toString())

        Log.d(Constants.NETWORKTAG, url)

        url.httpGet().responseString { request, response, result ->

            when (result) {
                is Result.Failure -> {
                    Log.d(Constants.NETWORKTAG, "Something went wrong ${result.error}")
                    callback(null)
                }
                is Result.Success -> {

                    val res = Gson().fromJson(result.value, AlbumInfoRequestList::class.java)
                    val album = albumPreview

                    res.tracklist?.forEach { it.albumID = album.id }

                    album.image = res.images[0]
                    album.songs = res.tracklist

                    if(res.artists.isNotEmpty()) album.artistID = res.artists[0].id

                    callback(album)
                }
            }
        }
    }
}
internal data class AlbumInfoRequestList(val images:List<Image>,var tracklist:List<Song>?, val artists: List<Artist>)
