package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class

import java.io.Serializable


/**
 * Created by ralphschnalzenberger on 30/10/2016.
 * A member of a an Artist (Band)
 */
data class Member(var active: Boolean,
                  var id: Int,
                  var artistID: Int,
                  var name: String,
                  var resURL: String?) : Serializable