package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest

/**
 * Created by ralphschnalzenberger on 13/11/2016.
 */
open class APICalls {

    companion object {
        //login credentials
        val CONSUMER_SECRET = "McwJlMcvnnmJZLEbZgQjNYPeJSJhfIVe"
        val CONSUMER_KEY = "MDODGpnRyzlNiMEuIkXQ"

        //URLs
        val BASEURL = "https://api.discogs.com"
        val SEARCH = "database/search"
        val ALBUM = "release_title"
        val BARCODE = "barcode"
        val RELEASE = "releases/"
        val ARTIST = "artists/"


        //Authentication String
        val AUTH_STRING = "&key=$CONSUMER_KEY&secret=$CONSUMER_SECRET"
    }
}