package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class

import java.io.Serializable
import java.util.*

/**
 * Created by ralphschnalzenberger on 30/10/2016.
 * Data representation of an artist/ a band
 */
data class Artist(var name: String,
                  var id: Int,
                  var resource_url: String?,
                  var releases_url: String?,
                  var profile: String?,
                  var image: Image?,
                  var members: List<Member>?) : Serializable