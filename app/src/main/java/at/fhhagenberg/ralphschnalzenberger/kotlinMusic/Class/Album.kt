package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class

import java.io.Serializable
import java.util.*

/**
 * Created by ralphschnalzenberger on 30/10/2016.
 * Data representation of an album
 */
data class Album(var image: Image?,
                 var artistID: Int?,
                 var songs: List<Song>?,
                 val id: Int,
                 val title: String,
                 val catno: String?,
                 val year: String?,
                 val resource_url: String?,
                 val country: String?,
                 val genre: List<String>,
                 val thumb: String?) : Serializable {

}
