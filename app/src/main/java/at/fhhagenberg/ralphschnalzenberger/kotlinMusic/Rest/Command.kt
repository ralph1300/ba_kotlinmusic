package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest

import javax.security.auth.callback.Callback

/**
 * Created by ralphschnalzenberger on 13/11/2016.
 */
interface Command<T> {
    fun execute(callback: (T) -> Unit)
}