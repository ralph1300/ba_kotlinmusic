package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Created by ralphschnalzenberger on 29/11/2016.
 * Imageloader
 */
class ImageLoader {

    companion object {
        /**
         * loads an image into a imageview
         * @param url the url of the image
         * @param imageView the imageview that image is loaded into
         */
        fun loadImage(url: String?, imageView: ImageView) {
            if (!url.isNullOrBlank()) {
                Picasso.with(imageView.context).load(url).fit().placeholder(R.drawable.ic_album_black_48dp).into(imageView, object : Callback {
                    override fun onSuccess() {
                    }

                    override fun onError() {
                        Log.d("ERROR", "COULD NOT LOAD IMAGE")
                    }
                })
            }
        }
    }
}