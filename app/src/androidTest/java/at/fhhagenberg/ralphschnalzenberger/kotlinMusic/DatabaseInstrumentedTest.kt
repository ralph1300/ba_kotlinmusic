package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.*
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database.MusicDataSource
import junit.framework.Assert
import junit.framework.TestCase
import org.jetbrains.anko.ctx
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by ralphschnalzenberger on 24/11/2016.
 */
@RunWith(AndroidJUnit4::class)
class DatabaseInstrumentedTest : TestCase(){

    var musicDataSource: MusicDataSource? = null

    override fun setUp() {
        super.setUp()
    }

    override fun tearDown() {
        super.tearDown()

    }

    @Test
    fun testDataLayer() {

        MusicDataSource.MusicDataSourceManager.createMusicDataSource(MusicDataSource(InstrumentationRegistry.getTargetContext()))
        musicDataSource = MusicDataSource.MusicDataSourceManager.musicDataSource

        //CREATE
        if(musicDataSource != null) {
            musicDataSource!!.saveAlbum(Album(Image(1, 1, "test", "test"), 1, listOf<Song>(Song(0, 1, "a", "1:12"),
                    Song(1, 1, "a", "1:12")), 1, "testalbum", "12345", "1994", "res/url", "aut", listOf("a", "b"), "thumb/nail"))
            musicDataSource!!.saveArtist(Artist("testartist", 1, "res/url", "resource/url", "profile", null,
                    listOf<Member>(Member(true, 1, 1, "mem1", "res1"), Member(false, 2, 1, "mem2", "res2"))))

            Assert.assertEquals(1, musicDataSource!!.getAllAlbums()?.size)
            Assert.assertEquals(1, musicDataSource!!.getAllArtists()?.size)

            //DELETE

            musicDataSource!!.deleteAlbum(1)
            musicDataSource!!.deleteArtist(1)

            Assert.assertEquals(0, musicDataSource!!.getAllAlbums()?.size)
            Assert.assertEquals(0, musicDataSource!!.getAllArtists()?.size)


            musicDataSource!!.dropAllTables()
            InstrumentationRegistry.getTargetContext().deleteDatabase("music.db")
        }

    }
}