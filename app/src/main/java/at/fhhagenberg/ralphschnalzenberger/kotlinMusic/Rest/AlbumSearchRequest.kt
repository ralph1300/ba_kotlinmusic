package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest

import android.util.Log
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Constants
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.getAs
import com.google.gson.Gson
import java.net.URL

/**
 * Created by ralphschnalzenberger on 09/11/2016.
 * Handles the network calls for searches
 */

class AlbumSearchRequest(val term: String, val type: Int) : Command<List<Album>?> , APICalls(){

    private companion object {
        //URL-Builder
        private fun URL_SEARCH_ALBUM(searchTerm: String): String = "$BASEURL/$SEARCH?$ALBUM=$searchTerm$AUTH_STRING"
        private fun URL_SEARCH_BARCODE(barcode: String): String = "$BASEURL/$SEARCH?$BARCODE=$barcode$AUTH_STRING"
    }

    override fun execute(callback: (List<Album>?) -> Unit) {
        var url = ""
        //get the URL
        when (type) {
            Constants.ALBUM -> url = (URL_SEARCH_ALBUM(term))
            Constants.BARCODE -> url = URL_SEARCH_BARCODE(term)
            else -> {
                Log.d(Constants.TAG, "Searching failed, make sure the right type is used.")
                callback(null)
            }
        }

        Log.d(Constants.NETWORKTAG, url)
        url.httpGet().responseString { request, response, result ->

            when (result) {
                is Result.Failure -> {
                    Log.d(Constants.NETWORKTAG, "Something went wrong ${result.error}")
                    callback(null)
                }
                is Result.Success -> {

                    val list = Gson().fromJson(result.value, AlbumRequestList::class.java)
                    //use case
                    //val exampleForOperatorOverloading = list[1]
                    callback(list.results)
                }
            }
        }
    }
}

//an internal data class to parse the searchresult properly - the pagination part of the json will be ignored
internal data class AlbumRequestList(val results: List<Album>) {
    //operator overloading -> now it is possible to access the list via
    // AlbumRequestList[position]
    // instead of
    // AlbumRequestList.results[position]
    operator fun get(position: Int): Album = results[position]
}
