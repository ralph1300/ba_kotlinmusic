package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Member
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R

/**
 * Created by ralphschnalzenberger on 19/11/2016.
 */
class ListAdapter<T>(val ctx: Context, val rID: Int, val data: List<T>?) : ArrayAdapter<T>(ctx, rID) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        var row: View? = convertView
        var lh: Holder?

        if (row == null) {
            val inflater = (ctx as Activity).layoutInflater
            row = inflater.inflate(rID, parent, false)
            lh = Holder()
            lh.name = row!!.findViewById(R.id.txtTitle) as TextView
            row.tag = lh
        } else {
            lh = row.tag as Holder
        }
        val holder = lh

        if(data != null) {
            when(data[position]) {
                is Song -> {
                    with(data[position] as Song) {
                        if (holder.name != null) {
                            val text = "$title : $duration"
                            holder.name!!.text = text
                        }
                    }
                }
                is Member -> {
                    with(data[position] as Member) {
                        if (holder.name != null) {
                            val isActive = if(active) "active" else "inactive"
                            val text = "$name : $isActive"
                            holder.name!!.text = text
                        }
                    }
                }
                else -> return null
            }
        }
        return row
    }

    override fun getCount(): Int {
        return data?.size ?: 0
    }
}

internal class Holder {
    var name: TextView? = null
}