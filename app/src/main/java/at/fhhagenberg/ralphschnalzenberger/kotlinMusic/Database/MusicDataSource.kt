package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.*
import java.util.*

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 * Datasource
 * @param context the context
 */
class MusicDataSource(val context: Context) {

    /**
     * the manager of the singelton of the datasource
     */
    object MusicDataSourceManager {

        private var dataSource: MusicDataSource? = null

        fun createMusicDataSource(source: MusicDataSource) {
            dataSource = source
        }

        val musicDataSource: MusicDataSource?
            get() = dataSource

    }

    private var mDatabase: SQLiteDatabase? = null
    private val mDatabaseHelper: MusicDatabaseHelper

    init {
        mDatabaseHelper = MusicDatabaseHelper(context)
    }

    /**
     * opens the database
     */
    private fun open() {
        try {
            mDatabase = mDatabaseHelper.writableDatabase
        } catch (ex: Exception) {
            mDatabase = null
        }
    }

    /**
    * closes the database
     */
    private fun close() {
        mDatabaseHelper.close()
        mDatabase = null
    }

    //LOAD

    /**
     * loads all albums
     * @return a list of all albums (nullable)
     */
    fun getAllAlbums() : List<Album>? {
        val albums = ArrayList<Album>()
        open()
        if (mDatabase != null) {
            try {
                val cursor = mDatabase!!.query(AlbumTable.TABLE_ALBUM,
                        arrayOf(AlbumTable.COLUMN_ARTISTID, AlbumTable.COLUMN_TITLE,
                                AlbumTable.COLUMN_CATNO, AlbumTable.COLUMN_COUNTRY,
                                AlbumTable.COLUMN_GENRE, AlbumTable.COLUMN_ID,
                                AlbumTable.COLUMN_IMAGE, AlbumTable.COLUMN_RESOURCEURL,
                                AlbumTable.COLUMN_THUMB, AlbumTable.COLUMN_YEAR), null, null, null, null, null)
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val album = cursorToAlbum(cursor)
                    if (album != null) {
                        album.songs = loadSongsForAlbumID(album.id) ?: ArrayList<Song>()
                        albums.add(album)
                    }
                    cursor.moveToNext()
                }
                cursor.close()
                return albums

            } catch (ex: Exception) {
                Log.d("DB","Database error: ${ex.message}")
                return null
            } finally {
                close()
            }
        }
        return null
    }

    /**
    * loads all artists
     * @return a list of all artists (nullable)
     */
    fun getAllArtists() : List<Artist>? {
        val artists = ArrayList<Artist>()
        open()
        if (mDatabase != null) {
            try {
                val cursor = mDatabase!!.query(ArtistTable.TABLE_ARTIST,
                        arrayOf(ArtistTable.COLUMN_ID,
                                ArtistTable.COLUMN_PROFILE, ArtistTable.COLUMN_IMAGE,
                                ArtistTable.COLUMN_NAME, ArtistTable.COLUMN_RELEASEURL,
                                ArtistTable.COLUMN_RESURL), null, null, null, null, null)
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val artist = cursorToArtist(cursor)
                    if (artist != null) {
                        artist.members = loadMembersForArtistID(artist.id)
                        artists.add(artist)
                    }
                    cursor.moveToNext()
                }
                cursor.close()
                return artists

            } catch (ex: Exception) {
                Log.d("DB","Database error: ${ex.message}")
                return null
            } finally {
                close()
            }
        }
        return null
    }

    /**
    * loads all songs for a id
     * @param id the id of the album
     * @return the songs of the album, otherwise nil
     */
    private fun loadSongsForAlbumID(id: Int): List<Song>? {

        val songList = ArrayList<Song>()
        open()
        if (mDatabase != null) {
            try {
                val cursor = mDatabase!!.query(
                        SongTable.TABLE_SONG,
                        arrayOf(SongTable.COLUMN_ID,
                                SongTable.COLUMN_ALBUMID, SongTable.COLUMN_DURATION,
                                SongTable.COLUMN_TITLE),
                        "${SongTable.COLUMN_ALBUMID} = ?",
                        arrayOf("$id"),
                        null,
                        null,
                        null
                )
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val song = cursorToSong(cursor)
                    if (song != null) {
                        songList.add(song)
                    }
                    cursor.moveToNext()
                }
                cursor.close()
                return songList

            } catch (ex: Exception) {
                Log.d("DB","Database error: ${ex.message}")
                return null
            } finally {
                close()
            }
        }
        return null
    }

    /**
    * loads all the members from an artist
     * @param id the id of the artist
     * @return a list of the members, otherwise null
     */
    private fun loadMembersForArtistID(id: Int): List<Member>? {
        val memberList = ArrayList<Member>()
        open()
        if (mDatabase != null) {
            try {
                val cursor = mDatabase!!.query(
                        MemberTable.TABLE_MEMBER,
                        arrayOf(MemberTable.COLUMN_ID,
                                MemberTable.COLUMN_ARTISTID, MemberTable.COLUMN_ACTIVE,
                                MemberTable.COLUMN_NAME, MemberTable.COLUMN_RESOURCEURL),
                        "${MemberTable.COLUMN_ARTISTID} = ?",
                        arrayOf("$id"),
                        null,
                        null,
                        null
                )
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    val member = cursorToMember(cursor)
                    if (member != null) {
                        memberList.add(member)
                    }
                    cursor.moveToNext()
                }
                cursor.close()
                return memberList

            } catch (ex: Exception) {
                Log.d("DB","Database error: ${ex.message}")
                return null
            } finally {
                close()
            }
        }
        return null
    }

    //CREATE
    /**
    * saves an album in the db
     * @param album the album to save
     */
    fun saveAlbum(album: Album) {
        val content = ContentValues()
        with(album) {
            content.put(AlbumTable.COLUMN_ID, id)
            content.put(AlbumTable.COLUMN_IMAGE, image.imageToString())
            content.put(AlbumTable.COLUMN_TITLE, title)
            content.put(AlbumTable.COLUMN_ARTISTID, artistID)
            content.put(AlbumTable.COLUMN_CATNO, catno)
            content.put(AlbumTable.COLUMN_COUNTRY, country)
            var genreString = ""
            genre.forEach { genreString += "$it;" }
            content.put(AlbumTable.COLUMN_GENRE, genreString)
            content.put(AlbumTable.COLUMN_RESOURCEURL, resource_url)
            content.put(AlbumTable.COLUMN_THUMB, thumb)
            content.put(AlbumTable.COLUMN_YEAR, year)
        }
        open()
        try {
            if (mDatabase != null) {
                mDatabase!!.insertOrThrow(AlbumTable.TABLE_ALBUM, null, content)
                if (album.songs != null) saveSongs(album.songs!!)
            }
        } catch (ex: Exception) {
            Log.d("DB","Database error: ${ex.message}")
            return
        } finally {
            close()
        }
    }

    /**
    * saves an artist
     * @param artist the artist to save
     */
    fun saveArtist(artist: Artist) {
        open()
        val content = ContentValues()
        with(artist) {
            content.put(ArtistTable.COLUMN_ID, id)
            content.put(ArtistTable.COLUMN_IMAGE, image.imageToString())
            content.put(ArtistTable.COLUMN_NAME, name)
            content.put(ArtistTable.COLUMN_PROFILE, profile)
            content.put(ArtistTable.COLUMN_RESURL, resource_url)
            content.put(ArtistTable.COLUMN_RELEASEURL, releases_url)
        }
        try {
            if (mDatabase != null) {
                mDatabase!!.insertOrThrow(ArtistTable.TABLE_ARTIST, null, content)
                if (artist.members != null) saveMember(artist.members!!)
            }
        } catch (ex: Exception) {
            Log.d("DB","Database error: ${ex.message}")
            return
        }finally {
            close()
        }
    }

    /**
    * saves songs into the db
     * @param songs a list of songs to save into the db
     */
    private fun saveSongs(songs: List<Song>) {
        open()
        songs.forEach {
            val content = ContentValues()
            with(it) {
                content.put(SongTable.COLUMN_ALBUMID, albumID)
                content.put(SongTable.COLUMN_DURATION, duration)
                content.put(SongTable.COLUMN_TITLE, title)
                try {
                    if (mDatabase != null) {
                        mDatabase!!.insertOrThrow(SongTable.TABLE_SONG, null, content)
                    }
                } catch (ex: Exception) {
                    Log.d("DB","Database error: ${ex.message}")
                    return
                }
            }
        }
        close()
    }

    /**
    * saves members into the db
     * @param members a list of members to save
     */
    private fun saveMember(members: List<Member>) {
        open()
        members.forEach {
            val content = ContentValues()
            with(it) {
                content.put(MemberTable.COLUMN_ID, id)
                content.put(MemberTable.COLUMN_ACTIVE, if(active) 1 else 0)
                content.put(MemberTable.COLUMN_ARTISTID, artistID)
                content.put(MemberTable.COLUMN_NAME, name)
                content.put(MemberTable.COLUMN_RESOURCEURL, resURL)
                try {
                    if (mDatabase != null) {
                        mDatabase!!.insertOrThrow(MemberTable.TABLE_MEMBER, null, content)
                    }
                } catch (ex: Exception) {
                    Log.d("DB","Database error: ${ex.message}")
                    return
                }
            }
        }
        close()
    }

    //DELETE
    /**
    * deletes the album and its songs
     * @param albumID the id of the album
     */
    fun deleteAlbum(albumID: Int) {
        open()
        if(mDatabase != null) {
            mDatabase!!.delete(AlbumTable.TABLE_ALBUM, "${AlbumTable.COLUMN_ID} = ?", arrayOf("$albumID"))
            mDatabase!!.delete(SongTable.TABLE_SONG, "${SongTable.COLUMN_ALBUMID} = ?", arrayOf("$albumID"))
        }
        close()
    }

    /**
     * deletes the artist and its members
     * @param artistID the id of the artist
     */
    fun deleteArtist(artistID: Int) {
        open()
        if(mDatabase != null) {
            mDatabase!!.delete(ArtistTable.TABLE_ARTIST, "${ArtistTable.COLUMN_ID} = ?", arrayOf("$artistID"))
            mDatabase!!.delete(MemberTable.TABLE_MEMBER, "${MemberTable.COLUMN_ARTISTID} = ?", arrayOf("$artistID"))
        }
        close()
    }

    //Cursor handler
    /**
    * converts a cursor to an album
     * @param cursor the cursor
     * @return if conversion succeeds, album
     */
    private fun cursorToAlbum(cursor: Cursor): Album? {
        try {
            val album = Album(
                    cursor.getString(6).stringToImage(),
                    cursor.getInt(0),
                    null,
                    cursor.getInt(5),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(9),
                    cursor.getString(7),
                    cursor.getString(3),
                    cursor.getString(4).split(';'),
                    cursor.getString(8)
            )

            return album
        } catch (ex: Exception) {
            Log.d("DB","Database error: ${ex.message}")
            return null
        }
    }

    /**
     * converts a cursor to an artist
     * @param cursor the cursor
     * @return if conversion succeeds, artist
     */
    private fun cursorToArtist(cursor: Cursor): Artist? {
        try {
            val artist = Artist(
                    cursor.getString(3),
                    cursor.getInt(0),
                    cursor.getString(5),
                    cursor.getString(4),
                    cursor.getString(1),
                    cursor.getString(2).stringToImage(),
                    null
            )
            return artist
        } catch (ex: Exception) {
            return null
        }
    }
    /**
     * converts a cursor to a song
     * @param cursor the cursor
     * @return if conversion succeeds, song
     */
    private fun cursorToSong(cursor: Cursor): Song? {
        try {
            val song = Song(
                    cursor.getInt(0),
                    cursor.getInt(1),
                    cursor.getString(3),
                    cursor.getString(2)
            )
            return song
        } catch (ex: Exception) {
            return null
        }
    }
    /**
     * converts a cursor to a member
     * @param cursor the cursor
     * @return if conversion succeeds, member
     */
    private fun cursorToMember(cursor: Cursor): Member? {
        try {
            val member = Member(
                    if (cursor.getInt(2) == 1)  true else false,
                    cursor.getInt(1),
                    cursor.getInt(0),
                    cursor.getString(3),
                    cursor.getString(4)
            )
            return member
        } catch (ex: Exception) {
            return null
        }
    }

    //drop tables
    fun dropAllTables() {
        open()
        mDatabaseHelper.dropDatabase(mDatabase)
        close()
    }

    /**
     * an string extension method
     * @return an image
     */
    fun String?.stringToImage(): Image? {
        if (this != null) {
            val parts = this.split(';')
            return Image(parts[0].toInt(), parts[1].toInt(), parts[2], parts[3])
        }
        return null

    }

    /**
     * an image extension method
     * @return a string
     */
    fun Image?.imageToString(): String? {
        if (this != null) {
            with(this) {
                return "$height;$width;$resource_url;$uri"
            }
        }
        return null
    }
}
