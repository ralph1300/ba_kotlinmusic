package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class

import java.io.Serializable

/**
 * Created by ralphschnalzenberger on 30/10/2016.
 * Data representation of an image
 */
data class Image(var height: Int,
                 var width: Int,
                 var resource_url: String,
                 var uri: String) : Serializable