package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Constants
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database.MusicDataSource
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.DetailActivity
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model.AlbumArtistListAdapter
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R
import com.github.clans.fab.FloatingActionButton
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.support.v4.intentFor

/**
 * Created by ralphschnalzenberger on 03/11/2016.
 */
class ArtistFragment : BaseFragment() {

    private var artists: List<Artist>? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_artist, container, false)

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        addOnclickListenerToButton((view?.findViewById(R.id.searchWithQR) as FloatingActionButton), Constants.ADD_ALBUM_BY_SCANNING_QR)
        addOnclickListenerToButton((view?.findViewById(R.id.searchWithText) as FloatingActionButton), Constants.ADD_ALBUM_BY_ENTERING_TEXT)
        (view?.findViewById(R.id.artistList) as RecyclerView).setHasFixedSize(true)

        val llm = LinearLayoutManager(act)
        llm.orientation = LinearLayoutManager.VERTICAL
        (view?.findViewById(R.id.artistList) as RecyclerView).layoutManager = llm
        showList()

    }

    override fun onResume() {
        super.onResume()

        val dataSource = MusicDataSource.MusicDataSourceManager.musicDataSource
        if (dataSource != null) {
            artists = dataSource.getAllArtists()
            showList()
        }
    }

    /**
    * shows list on fragment
     */
    private fun showList() {
        if (artists != null) {
            (view?.findViewById(R.id.artistList) as RecyclerView).adapter = AlbumArtistListAdapter<Artist>(act, artists!!) { artist ->
                startActivity(intentFor<DetailActivity<Artist>>().putExtra(Constants.DETAIL, artist))
            }
        }
    }
}