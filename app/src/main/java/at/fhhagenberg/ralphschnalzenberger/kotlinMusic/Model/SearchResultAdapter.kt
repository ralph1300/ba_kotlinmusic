package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R

/**
 * Created by ralphschnalzenberger on 13/11/2016.
 */
class SearchResultAdapter(val currActivity: Activity, val elements: List<Album>, val itemClick: (Album) -> Unit) : RecyclerView.Adapter<SearchResultHolder>() {

    override fun onBindViewHolder(holder: SearchResultHolder?, position: Int) {
        if (holder == null) return
        holder.bindSearchResultAlbum(elements[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SearchResultHolder? {

        if(parent != null){
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.cardview_searchresult_album, parent, false)
            return SearchResultHolder(itemView, itemClick)
        }
        return null
    }

    override fun getItemCount(): Int {
        return elements.size
    }
}