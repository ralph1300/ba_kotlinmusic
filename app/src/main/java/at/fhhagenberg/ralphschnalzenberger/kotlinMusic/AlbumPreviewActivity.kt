package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database.MusicDataSource
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model.ListAdapter
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R.id.*
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest.AlbumRequest
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest.ArtistRequest
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import org.jetbrains.anko.ctx
import org.jetbrains.anko.intentFor
//as kotlinx is not working anymore (2.2.17, findViewById is needed again until a solution is found)
class AlbumPreviewActivity : AppCompatActivity() {

    lateinit var previewAlbum: Album
    var loadedAlbum: Album? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_album_preview)

        previewAlbum = (intent.getSerializableExtra(Constants.ALBUM_PREVIEW) as Album)

        setupUI()
        loadSongsForAlbum();
    }

    private fun setupUI() {
        title = getString(R.string.choose)

        (findViewById(R.id.titleTextView) as TextView).text = previewAlbum.title
        (findViewById(R.id.infoTextView) as TextView).text = previewAlbum.country
        (findViewById(R.id.infoSecTextView) as TextView).text = previewAlbum.year

    }

    private fun loadSongsForAlbum() {
        val imgView = (findViewById(R.id.imageView) as ImageView)
        AlbumRequest(previewAlbum).execute { album ->
            if(album != null){
                loadedAlbum = album
                with(album) {
                    Picasso.with(imgView.context).load(image?.uri).fit().into(imgView, object : Callback {
                        override fun onSuccess() {
                        }
                        override fun onError() {
                            Log.d("ERROR", "COULD NOT LOAD IMAGE")
                        }
                    })
                    val adapter = ListAdapter<Song>(ctx, R.layout.listitem, songs)
                    (findViewById(R.id.songListView) as ListView).adapter = adapter
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_save -> {
                if(loadedAlbum != null) {
                    val dataSource = MusicDataSource.MusicDataSourceManager.musicDataSource
                    if(dataSource != null) {
                        dataSource.saveAlbum(loadedAlbum!!)
                        if (loadedAlbum?.artistID != null) {
                            ArtistRequest(loadedAlbum!!.artistID!!).execute { artist ->

                                if (artist != null) dataSource.saveArtist(artist)
                                ctx.startActivity(intentFor<MainActivity>())
                            }
                        }
                    }
                }
                return true
            }
            R.id.action_discard -> {
                    finish()
                    return true
                }

            else -> {
                //do nothing
                return true
            }
        }
    }
}
