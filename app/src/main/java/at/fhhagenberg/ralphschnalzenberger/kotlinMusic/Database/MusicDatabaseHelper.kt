package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 */
/**
 * represents the musicdatabasehelper
 * @param context the context
 */
class MusicDatabaseHelper(val context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, version) {

    companion object {
        val DATABASE_NAME = "music.db"
        val version = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        if (db != null) {
            AlbumTable.onCreate(db)
            ArtistTable.onCreate(db)
            MemberTable.onCreate(db)
            SongTable.onCreate(db)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (db != null) {
            AlbumTable.onUpgrade(db)
            ArtistTable.onUpgrade(db)
            MemberTable.onUpgrade(db)
            SongTable.onUpgrade(db)
        }
    }

    /**
    * drops all tables in the db
     * @param db the database
     */
    fun dropDatabase(db: SQLiteDatabase?) {
        if (db != null) {
            AlbumTable.dropTable(db)
            ArtistTable.dropTable(db)
            SongTable.dropTable(db)
            MemberTable.dropTable(db)
        }
    }
}