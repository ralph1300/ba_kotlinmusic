package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest

import android.util.Log
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Image
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Member
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Song
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Constants
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson

/**
 * Created by ralphschnalzenberger on 25/11/2016.
 */
class ArtistRequest(val id: Int) : Command<Artist?>, APICalls() {

    private companion object {
        //URL-Builder
        private fun URL_REQUEST_FULL_ARTIST(id: String): String = "$BASEURL/$ARTIST$id?$AUTH_STRING"
    }

    override fun execute(callback: (Artist?) -> Unit) {

        val url = URL_REQUEST_FULL_ARTIST(id.toString())

        Log.d(Constants.NETWORKTAG, url)

        url.httpGet().responseString { request, response, result ->

            when (result) {
                is Result.Failure -> {
                    Log.d(Constants.NETWORKTAG, "Something went wrong ${result.error}")
                    callback(null)
                }
                is Result.Success -> {

                    val res = Gson().fromJson(result.value, ArtistRequestDummy::class.java)
                    res.members.forEach { it.artistID = res.id }
                    val artist = Artist(res.name,res.id, res.resource_url, res.releases_url, res.profile, res.images?.elementAt(0), res.members)

                    callback(artist)
                }
            }
        }
    }
}
internal data class ArtistRequestDummy(val name: String, val id: Int, val resource_url: String?, val releases_url: String, val profile: String?, val images:List<Image?>?, var members: List<Member>)
