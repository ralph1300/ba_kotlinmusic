package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Created by ralphschnalzenberger on 13/11/2016.
 */
class SearchResultHolder(itemView: View?, val itemClick: (Album) -> Unit) :  RecyclerView.ViewHolder(itemView) {
    /**
    * binds a searchresult to the view
     * @param album the album that is to be bound
     */
    fun bindSearchResultAlbum(album: Album) {

        val imgView = itemView.findViewById(R.id.album_img) as ImageView
        val searchNameText = itemView.findViewById(R.id.searchResultNameView) as TextView
        val searchResultText = itemView.findViewById(R.id.searchResultCatNoView) as TextView

        with(album){
            if (!thumb.isNullOrBlank()) {
                Picasso.with(imgView.context).load(thumb).fit().placeholder(R.drawable.ic_album_black_48dp).into(imgView, object : Callback {
                    override fun onSuccess() {
                    }

                    override fun onError() {
                        Log.d("ERROR", "COULD NOT LOAD IMAGE")
                    }

                })
            }
            searchNameText.text = title
            searchResultText.text = catno
            itemView.setOnClickListener{ itemClick(this)}
        }
    }
}