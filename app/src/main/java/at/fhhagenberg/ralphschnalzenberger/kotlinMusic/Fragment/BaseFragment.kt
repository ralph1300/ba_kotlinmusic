package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Fragment


import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.support.v4.app.Fragment
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Constants
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.BarcodeCaptureActivity
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Rest.AlbumSearchRequest
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.SearchResultPreviewActivity
import com.github.clans.fab.FloatingActionButton
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.intentFor
import org.jetbrains.anko.support.v4.toast
import java.util.*

/**
 * Created by ralphschnalzenberger on 05/11/2016.
 * A Basefragment that the other fragments inherit from so that the code of adding an album is only implemented once
 */
open class BaseFragment : Fragment() {

    /**
     * The function creates a onclick function for the given button, the type of the function is determined by a type
     * @param button the button that the onclick should be added to
     * @param type the type of the button, see Constants.kt for more information
     */
    protected fun addOnclickListenerToButton(button: FloatingActionButton?, type: Int) {
        when (type) {
            Constants.ADD_ALBUM_BY_ENTERING_TEXT -> {
                button?.setOnClickListener {
                    alert(getString(R.string.enter_albumname)) {
                        customView {
                            verticalLayout {
                                val albumEdit = editText {
                                    singleLine = true
                                    hint = getString(R.string.album_hint)
                                }
                                positiveButton(getString(R.string.OK)) {
                                    if (albumEdit.text.toString() == "") {
                                        toast(getString(R.string.empty_search_hint))
                                    } else {
                                        executeAPISearch(albumEdit.text.toString(), Constants.ALBUM)
                                    }
                                }
                                negativeButton(getString(R.string.cancel)) { }
                            }
                        }
                    }.show()
                }
            }
            Constants.ADD_ALBUM_BY_SCANNING_QR -> {
                button?.setOnClickListener {
                    //intent without extra
                    val intent = Intent(context, BarcodeCaptureActivity::class.java)
                    startActivityForResult(intent, 1)
                }
            }
            else -> {
                error("This should not be possible.")
            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode != 1) return
        if (resultCode == Activity.RESULT_OK) {
            val barcode = data?.getStringExtra(Constants.GET_BARCODE_SCAN)
            if (barcode != null)executeAPISearch(barcode, Constants.BARCODE)
        }
    }

    /**
    * executes the search
     * @param term the searchterm
     * @param type, either a barcode or a text
     */
    private fun executeAPISearch(term: String, type: Int) {
        if (!(type == Constants.BARCODE || type == Constants.ALBUM)) return

        val progress = ProgressDialog(ctx)
        progress.setTitle(getString(R.string.loadTitle))
        progress.setMessage(getString(R.string.loadMessage))
        progress.setCancelable(false)

        progress.show()

        AlbumSearchRequest(term, type).execute { resultList ->
            if (resultList == null) {
                toast(getString(R.string.ERROR_NETWORKING))
                progress.dismiss()
            } else {
                var results: ArrayList<Album> = ArrayList(resultList)
                //max 15 results are shown
                if (results.size > Constants.MAX_LIST_LENGTH) {
                    results = ArrayList(results.subList(0, Constants.MAX_LIST_LENGTH))
                }
                //Intent with extra
                progress.dismiss()
                context.startActivity(intentFor<SearchResultPreviewActivity>().putExtra(Constants.SEARCH_RESULTS, results))
            }
        }
    }
}
