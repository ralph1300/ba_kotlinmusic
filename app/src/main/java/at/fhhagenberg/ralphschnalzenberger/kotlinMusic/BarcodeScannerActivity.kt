package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import android.Manifest.permission.CAMERA
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import eu.livotov.labs.android.camview.ScannerLiveView
import eu.livotov.labs.android.camview.scanner.decoder.zxing.ZXDecoder
import org.jetbrains.anko.alert
import org.jetbrains.anko.editText
import org.jetbrains.anko.toast
import org.jetbrains.anko.verticalLayout


/**
 * Activity for the multi-tracker app.  This app detects barcodes and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and ID of each barcode.
 */
class BarcodeCaptureActivity : AppCompatActivity() {

    private var flashStatus: Boolean = false
    private var camera:ScannerLiveView? = null
    private val PERMISSION_REQUEST_CODE = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(Constants.TAG, "BARCODE CAPTURE ACTIVITY")
        setContentView(R.layout.activity_barcode_scanner)

        if (!checkPermission()) requestPermission()

        camera = findViewById(R.id.camView) as ScannerLiveView
        camera?.scannerViewEventListener = object : ScannerLiveView.ScannerViewEventListener {

            override fun onScannerStarted(scanner: ScannerLiveView) {

            }

            override fun onScannerStopped(scanner: ScannerLiveView) {

            }

            override fun onScannerError(err: Throwable) {
                Toast.makeText(applicationContext, getString(R.string.QRERROR),Toast.LENGTH_LONG).show()
            }

            override fun onCodeScanned(data: String) {

                camera?.stopScanner()

                alert(getString(R.string.barcode_scanned) + " " + data) {
                    customView {
                        verticalLayout {
                            val barcodeEdit = editText {
                                hint = getString(R.string.barcode_hint)
                            }
                            positiveButton(getString(R.string.OK)) {
                                var barcode = data
                                if (barcodeEdit.text.toString() != "") {
                                    barcode = barcodeEdit.text.toString()
                                }
                                val intent = Intent()
                                intent.putExtra(Constants.GET_BARCODE_SCAN, barcode)
                                setResult(Activity.RESULT_OK, intent)
                                finish()
                            }
                            negativeButton(getString(R.string.try_again)) {
                                camera?.startScanner()
                            }
                        }
                    }
                }.show()
            }
        }

        (findViewById(R.id.btnFlash) as Button).setOnClickListener {
            View.OnClickListener {
                toggleFlashlight()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val decoder = ZXDecoder()
        decoder.scanAreaPercent = 0.5
        camera?.decoder = decoder
        camera?.startScanner()
    }


    override fun onPause() {
        camera?.stopScanner()
        super.onPause()
    }

    private fun toggleFlashlight() {
        flashStatus = !flashStatus
        camera?.camera?.controller?.switchFlashlight(flashStatus)
    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(applicationContext, CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(this, arrayOf(CAMERA), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {

                val cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (cameraAccepted)
                    toast("Permission Granted, Now you can access location data and camera")
                else {
                    toast("Permission Denied, You cannot access camera")
                    requestPermission()
                }
            }
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show()
    }
}