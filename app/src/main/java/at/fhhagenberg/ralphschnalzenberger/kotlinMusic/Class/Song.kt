package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class

import java.io.Serializable


/**
 * Created by ralphschnalzenberger on 30/10/2016.
 * Data representation of a song
 */
data class Song(var id: Int,
                var albumID: Int,
                var title: String,
                var duration: String) : Serializable
