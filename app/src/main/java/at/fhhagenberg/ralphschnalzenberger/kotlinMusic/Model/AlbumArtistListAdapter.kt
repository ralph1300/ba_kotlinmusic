package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Model

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Album
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.R

/**
 * Created by ralphschnalzenberger on 28/11/2016.
 */
class AlbumArtistListAdapter<T>(val currActivity: Activity, val elements: List<T>, val itemClick: (T) -> Unit) : RecyclerView.Adapter<AlbumArtistHolder<T>>() {

    override fun onBindViewHolder(holder: AlbumArtistHolder<T>?, position: Int) {
        holder?.bindArtistOrAlbum(elements[position])
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AlbumArtistHolder<T>? {
        if(parent != null){
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.cardview_albumartistoverview, parent, false)
            return AlbumArtistHolder(itemView, itemClick)
        }
        return null
    }
}