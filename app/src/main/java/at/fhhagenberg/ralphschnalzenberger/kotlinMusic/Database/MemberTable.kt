package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.database.sqlite.SQLiteDatabase

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 */
/**
 * represents the member table
 */
class MemberTable {

    companion object {
        val TABLE_MEMBER = "member"
        val COLUMN_ID = "id"
        val COLUMN_ACTIVE= "active"
        val COLUMN_ARTISTID = "artistID"
        val COLUMN_NAME = "name"
        val COLUMN_RESOURCEURL = "resourceURL"

        private val DATABASE_CREATE = "create table $TABLE_MEMBER (" +
                "$COLUMN_ID integer primary key," +
                "$COLUMN_ACTIVE integer," +
                "$COLUMN_ARTISTID integer," +
                "$COLUMN_RESOURCEURL text," +
                "$COLUMN_NAME text);"


        fun onCreate(database: SQLiteDatabase) {
            database.execSQL(DATABASE_CREATE)
        }

        fun onUpgrade(database: SQLiteDatabase){
            database.execSQL("DROP TABLE IF EXISTS $TABLE_MEMBER");
            onCreate(database);
        }
        fun dropTable(database: SQLiteDatabase) {
            database.execSQL("DROP TABLE IF EXISTS $TABLE_MEMBER");
        }
    }
}
