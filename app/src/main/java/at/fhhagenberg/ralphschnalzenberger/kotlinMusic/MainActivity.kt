package at.fhhagenberg.ralphschnalzenberger.kotlinMusic


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database.MusicDataSource
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Fragment.AlbumFragment
import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Fragment.ArtistFragment
import org.jetbrains.anko.ctx
import java.util.*

class MainActivity : AppCompatActivity() {


    private var artistFragment: ArtistFragment = ArtistFragment()
    private var albumFragment: AlbumFragment = AlbumFragment()

    lateinit var tabs: TabLayout
    lateinit var pager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        tabs = findViewById(R.id.tabs) as TabLayout
        pager = findViewById(R.id.pager) as ViewPager
        setupUI()
        setupViewPager()
        createDatabase()



        tabs.setupWithViewPager(pager)
    }

    /**
    * creates the database
     */
    private fun createDatabase() {
        MusicDataSource.MusicDataSourceManager.createMusicDataSource(MusicDataSource(ctx))

        //ctx.deleteDatabase("music.db")
    }

    private fun setupUI() {
        supportActionBar?.title = getString(R.string.titleName)
    }

    private fun setupViewPager() {
        val adapter = ViewPagerAdapter(supportFragmentManager)

        adapter.addFragment(artistFragment, getString(R.string.Artist))
        adapter.addFragment(albumFragment, getString(R.string.Album))

        pager.adapter = adapter
    }
}

internal class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return mFragmentList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence {
        return mFragmentTitleList[position]
    }
}