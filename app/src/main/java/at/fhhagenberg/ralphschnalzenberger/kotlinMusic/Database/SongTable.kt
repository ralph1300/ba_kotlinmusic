package at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Database

import android.database.sqlite.SQLiteDatabase

/**
 * Created by ralphschnalzenberger on 23/11/2016.
 */
/**
 * represents the song table
 */
class SongTable {

    companion object {
        val TABLE_SONG = "song"
        val COLUMN_ID = "id"
        val COLUMN_ALBUMID = "artistID"
        val COLUMN_TITLE = "title"
        val COLUMN_DURATION = "duration"

        private val DATABASE_CREATE = "create table $TABLE_SONG (" +
                "$COLUMN_ID integer primary key autoincrement," +
                "$COLUMN_ALBUMID integer," +
                "$COLUMN_DURATION text," +
                "$COLUMN_TITLE text);"


        fun onCreate(database: SQLiteDatabase) {
            database.execSQL(DATABASE_CREATE)
        }

        fun onUpgrade(database: SQLiteDatabase){
            database.execSQL("DROP TABLE IF EXISTS $TABLE_SONG");
            onCreate(database);
        }

        fun dropTable(database: SQLiteDatabase) {
            database.execSQL("DROP TABLE IF EXISTS $TABLE_SONG");
        }
    }
}
/*
data class Song(var id: Int,
                var albumID: Int,
                var title: String,
                var duration: String)
 */