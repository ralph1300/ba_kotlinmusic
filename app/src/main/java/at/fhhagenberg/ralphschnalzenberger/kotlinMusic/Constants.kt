package at.fhhagenberg.ralphschnalzenberger.kotlinMusic

import at.fhhagenberg.ralphschnalzenberger.kotlinMusic.Class.Artist

/**
 * Created by ralphschnalzenberger on 29/10/2016.
 * Contains necessary static information for the application like URLs
 */
class Constants {

    //use of companion object allows static-like use of class
    companion object {

        //FloatingActionButtonActionTypes

        val ADD_ALBUM_BY_ENTERING_TEXT = 0
        val ADD_ALBUM_BY_SCANNING_QR   = 1

        //Searchtypes
        val ALBUM   = 0
        val BARCODE = 1

        //LOG-Tags
        val TAG = "INFO"
        val NETWORKTAG = "NETWORK"

        //Misc
        val MAX_LIST_LENGTH = 15

        //INTENT
        val SEARCH_RESULTS = "SEARCHRESULT"
        val GET_BARCODE_SCAN = "BARCODESCAN"
        val ALBUM_PREVIEW = "ALBUMPREVIEW"
        val DETAIL = "DETAIL"


    }

}